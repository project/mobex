Mobex Module for Drupal 
------------------------------------------------------------------------------------
Features:
------------------------------------------------------------------------------------
- Adds Mobex Menu link on the top Menu Bar to access app.gomobex.com:9001
- Adds Mobex Menu to the content items to publish and unpublish content to Mobex Server

------------------------------------------------------------------------------------
Before enabling the module, do the following as described -
------------------------------------------------------------------------------------
STEP 1 - Install the following modules 
- Feeds
- Feeds XPath Parser
- Job Scheduler
- Features
- Chaos Tools
- Views
- Views datasource

Go to Modules -> Install a new module 
Upload the module archive and install
Enable the module by selecting the checkbox under enabled column of the modules
Save Configuration
--------------------------------------------------------------------------------------

Go to STEP 3b for the instructions to enable the module for existing content type 

Instructions for enabling the module for non-existing content types 
--------------------------------------------------------------------------------------
STEP 2 - Create a content type.
Go to Structure -> Content types -> Add content type.
Eg: Name: Mobex Article
    Description: Article content type for Mobex
    Title field label - title
    Save and add fields 
    Give the below field labels, select a field type, select a widget, Save.

    LABEL              MACHINE NAME          FIELD TYPE                 WIDGET
    ----------------------------------------------------------------------------------------
    title*             title                 Long text and summary      Text area with a summary
    body*              body
    mobexId*                                 Text                       Text field
    mobexState*                              Text                       Text field  ----> Default value:unpublished
    imageExpandedUrl                         Text                       Text field
    imageUrl                                 Text                       Text field
    imageCaption                             Text                       Text field
    byline                                   Text                       Text field
    datePublished                            Text                       Text field
    extendedView                             Text                       Text field    Size of the field - 1000

* required fields
- Make sure the labels are the same as the above as they need to match with Mobex article format.
- Machine names are auto generated. 
- To have more than one value for a field, Go to manage fields -> Select a field -> Select the "Number of values" 
drop down -> Save settings. Eg: byline can have two values.
- The published date can be any valid php date format. Refer to http://www.php.net/manual/en/datetime.formats.php for the 
supported date and time formats.

-----------------------------------------------------------------------------------------------------
STEP 3a - Create a View 
Make sure the following modules are installed and enabled - Views, Views_datasource, chaos_tools.
Download the zip folder of the required modules)
- Go to Modules -> Install a new module -> Upload the zip folder -> Install
- Look for the module name under Modules -> check -> Save 

Go to Structure -> Views -> Add a new view
Eg: - View name: Mobex Article View
    - Select Show "Content" of type "Mobex Article"
    - Check "Create a page"
    - Page Title is auto-filled same as View name
    - Path: /mobex-article-view/% (% refers to a contextual filter. Eg: Node id)
    - Display format "JSON Data document"
    - Continue and edit
    - Format -> Settings -> Uncheck "Views API mode"
    - Apply 
    - Fields -> 
    Add Content:Nid
    Add (check all the mobex-article fields created in the above section 
      Eg: Content: datePublished 
          Appears in: node:mobex_article.)
    - Fields -> Content:Body -> Formatter -> Plain text -> Apply
    - Check the Sort Criteria as desired as Post date(desc)
    - PAGER - Display a specified number of items | 50 items
    - Apply and click Apply for all the Configure:field dialogue boxes.
    - Advanced -> Contextual filters -> Add -> Check Content: Nid 
    Under the section "WHEN THE FILTER VALUE IS NOT IN THE URL" - select "Display all results for the specified field"
    Under the section "Exceptions" -> Exception value = all
    - Apply
    - Save - Go to STEP 4

----------------------------------------------------------------------------------------------------
STEP 3b - Create a View 
Let's assume there is an existing content type "Feed item"
Go to Structure -> Views -> Add a new view
Eg: - View name: Feed View
    - Select Show "Content" of type "Feed item"
    - Check the "Create a page"
    - Page Title is auto-filled same as View name
    - Path: /feed-item-view/% (% refers to a contextual filter. Eg: Node id)
    - Display format "JSON Data document"
    - Continue 
    - Format settings -> Uncheck "Views API mode"
    - Apply
    - Fields ->
    Add Content:Nid
    Add (check all the feed item fields required)
    Mobex article json format is -
    {
      "id":"", ----> Node id
      "title":"",
      "byline":"",
      "datePublished":"",
      "imageCaption":"",
      "imageExpandedUrl":"",
      "imageUrl":"",
      "mobexId":"",
      "mobexState":"unpublished",
      "body":"",
      "extendedView":""
    }
    change the labels of each field to match the mobex article json format
    Fields -> Content:body -> Fomratter -> Plain text 
    Eg: Assume there is field called createdDate, change it to datePublished

    - Check the Sort Criteria as desired as Post date(desc)
    - PAGER - Display a specified number of items | 50 items
    - Apply and click Apply for all the Configure:field dialogue boxes.
    - Advanced -> Contextual filters -> Add -> Check Content: Nid
    Under the section "WHEN THE FILTER VALUE IS NOT IN THE URL" - select "Display all results for the specified field"
    Under the section "Exceptions" -> Exception value = all
    - Apply and Save

----------------------------------------------------------------------------------------------------
STEP 4 - Create new content (To import content, Skip this step and follow the instructions under Import-feeds-to-mobex.txt)
Eg: Go to Content -> Add Content -> Mobex Article
    Add content and Save

To verify that the json generated by the view created in STEP 3 is in the right format,
Go to http://<baseUrl>/mobex-article-view/<nodeId> and verify that the json displayed is of the below format -
{
   "nodes":[
      {
         "node":{
            "id":"", ---> node id
            "title":"",
            "byline":"",
            "datePublished":"",
            "imageCaption":"",
            "imageExpandedUrl":"",
            "imageUrl":"",
            "mobexId":"",
            "mobexState":"unpublished",
            "body":"",
            "extendedView":""
         }
      }
   ]
}

------------------------------------------------------------------------------------
STEP 5 - Instructions to install Mobex Module - 
- Create a zip folder of mobex folder
- Go to Modules section 
- Click on install new module 
- Upload the mobex zip folder by choosing a file 
- Click on Install 
- Now look for Mobex module in the modules section and check the box 
- Save Configuration

-----------------------------------------------------------------------------------
STEP 6 - Configuration of the mobex module 
- Go to Configuration -> Mobex
- Verify the configuration 
  Base URL - Put the url of Mobex Server Eg: http://app.gomobex.com:9001
  API path - /api
  Enter Application id, Key, Source id, Source password based on the app ypu want to connect with Mobex
  View -> Path - path of the view created in STEP 3 Eg: /mobex-article-view
  Mapping -> Select a content type. 
             Put the respective machine names generated for the fields of the Content type 
             "Mobex Article" created in STEP 1.
- Save Configuration 

------------------------------------------------------------------------------------
STEP 7 - Clear the cache - 
------------------------------------------------------------------------------------
If any of the above steps fails to produce the expected outcome, clear the cache of the drupal. 
To clear the cache - 
- Go to Configuration -> Development -> Performance -> Clear all caches

------------------------------------------------------------------------------------
STEP 8 - Test the module
- Go to Content 
- Click on any of the created/imported Content items of the type "Mobex Article"
- Mobex -> Publish/Unpublish


